from flask import *
from pymongo import *
import reviews
import json
import StringIO
import uuid
import types
import time
import datetime
import pytz
import os

app = Flask(__name__)
app.debug = True

# converts newegg's time values to 

#Function to setup mongoclient for db access
#Somewhere we will have to define the collections either in the 
#url, or we can use the same collection throughout and 
#hard code its name
def setup_mongoclient(isLocal):
  if(isLocal):
    return MongoClient()
  else:
    return MongoClient(os.getenv('MONGOLAB_URI'))

def setup_database(client, isLocal):
  if(isLocal):
    return client['local']
  else:
    return client.get_default_database()

def isLocal():
  network_db = os.getenv('MONGOLAB_URI')
  if(network_db == None):
    return True
  else:
    return False

#Bring up Documentation
@app.route("/", methods=['GET', 'POST'])
def default():
	if request.method == 'POST':
		return postRoot()
	if request.method == 'GET':
		return getRoot()


@app.route("/<loginId>/products", methods=['GET', 'POST'])
def getUserProducts(loginId):
	if request.method == 'POST':
		productsInJson = request.data
		return postProducts(loginId, productsInJson)
	if request.method == 'GET':
		return getProducts(loginId)


@app.route("/<loginId>/products/reviews", methods=['GET'])
def getReviews(loginId):
	stars = request.args.get('stars-or-less', None)
	time = request.args.get('reviews-since', None)
	return getProductsReviews(loginId, stars, time)


@app.route("/<loginId>/products/<productId>/reviews", methods=['GET'])
def getProductReview(loginId, productId):
	stars = request.args.get('stars-or-less', None)	
	time = request.args.get('reviews-since', None)
	return getOneProductReview(loginId, productId, stars, time)


# return response of ID creation in plain text
def postRoot():
	loginId = createId()

	products = []
	post = {"loginId": loginId, "products": products}

	local = isLocal()
	client = setup_mongoclient(local)
	db = setup_database(client,local)

	db.hi.insert(post)

	client.close()

	return loginId


# create UUID and return it
def createId():
	# generates a uuid for the user to use in a hexidecimal string
	return str(uuid.uuid4())


# return api documentation in text/plain
def getRoot():
	return redirect("https://bitbucket.org/sreisman/" +\
		"newegg-api-webservice/overview")


# return list of all products for this Id. See README Appendix 1
def postProducts(loginId, productsInJson):
	# reviews in string
	all_reviews = reviews.GET(productsInJson)

	# JSON object
	json_reviews = json.loads(all_reviews)
	# only products
	json_products = json_reviews['products']

	local = isLocal()
	client = setup_mongoclient(local)
	db = setup_database(client,local)

	db.hi.update({"loginId": loginId}, {"$set": {"products": json_products}})

	client.close()

	resp = Response(productsResponse(loginId), status=200,\
		mimetype='application/json')

	return resp

# creates a products response(only product-ids) from products document
def productsResponse(loginId):
	local = isLocal()
	client = setup_mongoclient(local)
	db = setup_database(client,local)

	dictionary = db.hi.find_one({'loginId' : loginId}, {"products":1, "_id":0})

	for item in dictionary.values():
                for item2 in item:
                        for item3 in item2.keys():
                                if (item3 != "product"):
                                        del(item2[item3])

	client.close()
	
	return json.dumps(dictionary, indent = 4, separators=(',', ': '),sort_keys=True)

# return list of all products for this Id. See README Appendix 1
def getProducts(loginId):

	resp = Response(productsResponse(loginId), status=200,\
		mimetype='application/json')

	return resp



# Make newegg's datetime to PST and then convert it to GMT
def toGMT(dt_str):
	local = pytz.timezone ("America/Los_Angeles")
	naive = datetime.datetime.strptime (dt_str, "%m/%d/%Y %I:%M:%S %p")	
	local_dt = local.localize(naive, is_dst=None)
	utc_dt = local_dt.astimezone (pytz.utc)
	return utc_dt.strftime ("%m/%d/%Y %I:%M:%S %p")



# Convert GMT to UNIX timestamp
def getUnixTimestamp(dt_str):
	gmt = toGMT(dt_str)	
	return time.mktime(datetime.datetime.strptime(gmt, "%m/%d/%Y %I:%M:%S %p").timetuple())	



# Returns all reviews with stars-or-less and reviews-since for all products
def getProductsReviews(loginId, stars, time):	
	if (stars == None):
		stars = 5
	else:		
		stars = int(stars)

	if (time == None):
		time = 0 
	else:
		time = float(time)
	
	local = isLocal()
	client = setup_mongoclient(local)
	db = setup_database(client,local)

	cursor = db.hi.find_one({'loginId' : loginId}, \
		{"products":1, "_id":0})
	

	if(cursor==None):
		return "No products found.."	


	finalOutput = []

	for products in cursor.values():
		for eachProduct in products:				
			output = {}
			output["product"] = eachProduct['product']
			revList = []
			
			for eachReview in eachProduct['Reviews']:					
				if ( (eachReview['Rating'] <= stars) and (getUnixTimestamp(eachReview['PublishDate']) >= time) ):
					revList.append(eachReview)				
			
			output["Reviews"] = revList
		
			finalOutput.append(output)

	client.close()
	
	return json.dumps(finalOutput, indent = 4, separators=(',', ': '),sort_keys=True) 
	



# Returns all reviews with stars-or-less and reviews-since for single product
def getOneProductReview(loginId, productId, stars, time):
	if (stars == None):
		stars = 5
	else:		
		stars = int(stars)

	if (time == None):
		time = 0 
	else:
		time = float(time)
	

	local = isLocal()
	client = setup_mongoclient(local)
	db = setup_database(client,local)

	cursor = db.hi.find_one({'loginId' : loginId, 'products.product':productId}, \
		{"products.$":1, "_id":0})
	
	if(cursor==None):
		return "Product not found.."	

	#output = {"product": productId}
	revList = []		
	
	
	for products in cursor.values():
		for eachProduct in products:				
			if eachProduct['product'] == productId:
				for eachReview in eachProduct['Reviews']:					
					if ( (eachReview['Rating'] <= stars) and (getUnixTimestamp(eachReview['PublishDate']) >= time) ):
						revList.append(eachReview)				
	
	client.close()
	#output["Reviews"] = revList
	return json.dumps(revList, indent = 4, separators=(',', ': '),sort_keys=True) 
	
	

