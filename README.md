This is a RESTful web service for newegg products. This API allows product manufacturers to query all reviews of their products sold on newegg.com

Webservice URL:  

	http://newegg-api-webservice.herokuapp.com/

to run locally:  
virtualenv venv --distribute  
source venv/bin/activate  
pip install Flask gunicorn  
(probably one other thing to install with pip, get that too)  

foreman start  

API Documentation:
==================

### POST http://newegg-api-webservice.herokuapp.com/  
> Returns a new login-id(UUID) to the user to use in text/plain  
>
> _Request body_: empty  
> _Response body_: UUID in text/plain (e.g. "8b163c22-20a7-469e-8ac7-1c7a6acefd32")  

### GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/  
> Gets a list of this id's products in JSON format.  
>
> _Response body_: List of products in JSON format. See appendix 1

### POST http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/    
> Add new products in JSON format to the list of products for this id. If  
> a product or all products already exist in this id's list, the  
> product(s) will not be added.  
>
> _Request body_: List of products in JSON format. See appendix 1  
> _Response body_: List of products in JSON format. See appendix 1  

### GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/reviews?stars-or-less=__{stars-or-less}__&reviews-since=__{reviews-since}__  
> Get reviews for all products with the specified parameters
>
> _Response body_: List of reviews in JSON format. See appendix 2  

### GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/__{product-id}__/reviews?stars-or-less=__{stars-or-less}__&reviews-since=__{reviews-since}__
> Get reviews for this product with the specified parameters
>
> _Response body_: List of reviews in JSON format. See appendix 3  

### Parameters
> __{login-id}__ - Required. Your login-id given to you when you POST to the base URI.  
> __{stars-or-less}__ - Not required. A number between 1 and 5 to return all reviews with {stars-or-less} stars or less.  
> __{reviews-since}__ - Not required. A timestamp in javascript form (number of seconds since Jan. 1, 1970 to return all reviews since {reviews-since}.  

### Appendices  
	Appendix 1:  
	{  
	  "products":[  
	    { "product":"N82E16832152085" }  
	    { "product":"N82E16883103823" }  
	  ]  
	}  

	Appendix 2:  
        [
          {
            "Reviews": [
              {
                ...
              }
            ],
            "product": "N82E16832152085"
          },
          {
            "Reviews": [
              {
                ...
              }
            ],
            "product": "N82E16883103823"
          }
        ]
	    
	Appendix 3:  
        [
          {
             ... // review
          },
	  {
             ... // review
          }
        ]

## Examples using curl:
get uuid:  
curl -X POST -H "Content-Type: application/json" http://newegg-api-webservice.herokuapp.com -d ""  

get products:  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products  

post products:  
curl -X POST - "Content-Type: application/json" http://newegg-api-webservice.herokuapp.com/{UUID}/products -d '{"products":[{"product":"N82E16832152085"},{"product":"N82E16883131016"}]}'  

get products:  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products  

modify products:  
curl -X POST -H "Content-Type: application/json" http://newegg-api-webservice.herokuapp.com/{UUID}/products -d '{"products":[{"product":"N82E16832152085"},{"product":"N82E16883103823"}]}'  

get products:  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products  

all reviews for products: curl -X GET http://newegg-api-webservice.herokuapp.com/a9abfc8a-5309-4719-aa0a-d4a2e5ffbd58/products/reviews

5 stars or less for products:  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products/reviews?stars-or-less=5  

3 stars or less for products:  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products/reviews?stars-or-less=3  

reviews since 10/1/2013 (1380585600 unix time):  
curl -X GET http://newegg-api-webservice.herokuapp.com/{UUID}/products/reviews?reviews-since=1380585600  

reviews since 10/1/2013 and 3 stars or less:  
curl -X GET htp://newegg-api-webservice.herokuapp.com/{UUID}/products/reviews?stars-or-less=3\&reviews-since=1380585600  

reviews since 10/1/2013 and 3 stars or less for product "N82E16883103823":  
curl -X GET htp://newegg-api-webservice.herokuapp.com/{UUID}/products/N82E16883103823/reviews?stars-or-less=3\&reviews-since=1380585600  

reviews for product "N82E16883103823":  
curl -X GET htp://newegg-api-webservice.herokuapp.com/{UUID}/products/N82E16883103823/reviews  
